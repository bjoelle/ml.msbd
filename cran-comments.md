## Test environments
* local OS X install, R 4.0.2
* r-hub environments
* win-builder (devel)

## R CMD check results

0 errors | 0 warnings | 0 notes

## Reverse dependencies

There are no reverse dependencies (checked with ``devtools::revdep``).
